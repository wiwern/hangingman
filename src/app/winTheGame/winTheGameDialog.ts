import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';

@Component({
    selector: 'win-the-game-dialog',
    styleUrls: ['./win-the-game-dialog.scss'],
    templateUrl: './win-the-game-dialog.html',
})
export class WinTheGameDialog {
  public timeOnGame;
  constructor(
    public dialogRef: MatDialogRef<WinTheGameDialog>,
    @Inject(MAT_DIALOG_DATA) public data) {}

    public submit(): void {
      this.dialogRef.close();
    }

    public sumbitPlay(): void {
      this.dialogRef.close('playAgain');
    }
}