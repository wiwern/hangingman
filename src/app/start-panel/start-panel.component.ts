import { Component, OnInit } from '@angular/core';
import { GameService } from '../game.service';

@Component({
  selector: 'app-start-panel',
  templateUrl: './start-panel.component.html',
  styleUrls: ['./start-panel.component.scss']
})
export class StartPanelComponent {

  constructor(private gameService: GameService) { }


  public startTheGame(): void {
    this.gameService.changeComponent('game');
  }

}
