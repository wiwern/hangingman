import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { WinTheGameDialog } from './../winTheGame/winTheGameDialog';
import { GameService } from '../game.service';

@Component({
  selector: 'app-game',
  styleUrls: ['./game.component.scss'],
  templateUrl: './game.component.html',
})
export class GameComponent implements OnInit {
  @ViewChild('canvas', { static: true })
  public canvas: ElementRef<HTMLCanvasElement>;
  public alphabet: Array<string> = [
      'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
      'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
      'z', 'x', 'c', 'v', 'b', 'n', 'm',
  ];
  private ctx: CanvasRenderingContext2D;
  public arrayOfWords: Array<string> = [];
  public level = 1;
  public word: string;
  public wordArray;
  public guessingLetter;
  private fail = 0;
  public gameOver = false;
  public startGame = 0;
  public endGame = 0;
  public timeOnGame = '';
  public progresBarValue = 0;

  constructor(private http: HttpClient,
              private dialog: MatDialog,
              private gameService: GameService) {
    this.http.get('http://localhost:4200/assets/answers.json').subscribe((data: any) => {
      const arrayOfIndexes = [];
      let i = 0;
      for (i; i <= 4; i++) {
        const numberOfAnswer = Math.floor(Math.random() * (29 - 0)) + 0;
        const index = arrayOfIndexes.includes(numberOfAnswer, 0);
        if (index === false) {
          arrayOfIndexes.push(numberOfAnswer);
        } else {
          i--;
        }
      }
      console.log(arrayOfIndexes);
      for (const i of arrayOfIndexes) {
        const word = data.answers[i];
        this.arrayOfWords.push(word);
      }
      console.log(this.arrayOfWords);

      this.word = this.arrayOfWords[0];
      this.goToTheWord(this.word);
    });
  }

  public ngOnInit(): void {
    const data = new Date();
    const milliseconds = data.valueOf();
    this.startGame = milliseconds;

    this.ctx = this.canvas.nativeElement.getContext('2d');
    const draws = [
      'gallows',
      'head',
      'body',
      'rightHarm',
      'leftHarm',
      'rightLeg',
      'leftLeg',
      'rightFoot',
      'leftFoot',
   ];
    this.drawAMan(draws);
    this.level = 1;
  }

  private drawAMan(draws: Array<string>): void {

   draws.forEach((part) => {
    switch (part) {
      case 'gallows' :
        this.ctx.strokeStyle = '#444';
        this.ctx.lineWidth = 10;
        this.ctx.beginPath();
        this.ctx.moveTo(175, 225);
        this.ctx.lineTo(5, 225);
        this.ctx.moveTo(40, 225);
        this.ctx.lineTo(25, 5);
        this.ctx.lineTo(100, 5);
        this.ctx.lineTo(100, 25);
        this.ctx.stroke();
        break;

      case 'head':
        this.ctx.lineWidth = 5;
        this.ctx.beginPath();
        this.ctx.arc(100, 50, 25, 0, Math.PI * 2, true);
        this.ctx.closePath();
        this.ctx.stroke();
        break;

      case 'body':
        this.ctx.beginPath();
        this.ctx.moveTo(100, 75);
        this.ctx.lineTo(100, 140);
        this.ctx.stroke();
        break;

      case 'rightHarm':
        this.ctx.beginPath();
        this.ctx.moveTo(100, 85);
        this.ctx.lineTo(60, 100);
        this.ctx.stroke();
        break;

      case 'leftHarm':
        this.ctx.beginPath();
        this.ctx.moveTo(100, 85);
        this.ctx.lineTo(140, 100);
        this.ctx.stroke();
        break;

      case 'rightLeg':
        this.ctx.beginPath();
        this.ctx.moveTo(100, 140);
        this.ctx.lineTo(80, 190);
        this.ctx.stroke();
        break;

      case 'rightFoot':
        this.ctx.beginPath();
        this.ctx.moveTo(82, 190);
        this.ctx.lineTo(70, 185);
        this.ctx.stroke();
        break;

      case 'leftLeg':
        this.ctx.beginPath();
        this.ctx.moveTo(100, 140);
        this.ctx.lineTo(125, 190);
        this.ctx.stroke();
        break;

      case 'leftFoot':
        this.ctx.beginPath();
        this.ctx.moveTo(122, 190);
        this.ctx.lineTo(135, 185);
        this.ctx.stroke();
        break;
    }
   });
  }

  private showGameOver(): void {
    this.gameOver = true;
  }

  private showWinningTheGame(): void {
    const data = new Date();
    const  milliseconds = data.valueOf();
    this.endGame = milliseconds;

    const time = this.endGame - this.startGame;
    const seconds = Math.round(time / 1000);
    const minutes = Math.round(seconds / 60);
    const modulo = seconds % 60;
    this.timeOnGame = `Time on game: ${minutes} minutes, ${modulo} seconds`;

    const dialogRef = this.dialog.open(WinTheGameDialog, {
      width: '600px',
      data: {timeOnGame: this.timeOnGame},
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      if (result === 'playAgain') {
        this.playAgain();
      } else {
        this.gameService.changeComponent('start');
      }
    });
  }

  public checkThisLetter(letter: string): void {
    if (letter === this.guessingLetter.value) {
      const index = this.wordArray.indexOf(this.guessingLetter);
      this.guessingLetter = this.wordArray[index + 1];
        this.wordArray.splice(index, 1, {'letter': letter});
      if ((index + 1) === this.wordArray.length) {
        this.level = this.level + 1;
        this.progresBarValue  = this.progresBarValue + 20;
        this.goToANextLevel();
      }
    } else {
      this.fail++;
      this.triggerFail();
    }
  }

  private goToTheWord(word: string): void {
    console.log(word);
    length = word.length;
    const wordArray = [];

    for (let i = 0; i < length; i++) {
      wordArray.push(
        {'key': ' ',
        'value': word.charAt(i)
        }
      )
    }
    console.log(wordArray);
    this.wordArray = wordArray;

    this.guessingLetter = this.wordArray[0];
  }

  private goToANextLevel(): void {
    if (this.level >= 6) {
      this.showWinningTheGame();
    } else {
      this.word = '';
      this.guessingLetter = null;
      this.fail = 0;
      const index = this.level - 1;
      const word = this.arrayOfWords[index];
      const draws = [
        'gallows',
        'head',
        'body',
        'rightHarm',
        'leftHarm',
        'rightLeg',
        'leftLeg',
        'rightFoot',
        'leftFoot',
      ];
      this.drawAMan(draws);
      this.goToTheWord(word);
    }
  }

  private playAgain(): void {
    this.fail = 0;
    this.arrayOfWords = [];
    this.level = 1;
    this.timeOnGame = '';
    this.progresBarValue = 0;

    this.http.get('http://localhost:4200/assets/answers.json').subscribe((data: any) => {
        const arrayOfIndexes = [];
        let i = 0;
        for (i; i <= 4; i++) {
          const numberOfAnswer = Math.floor(Math.random() * (29 - 0)) + 0;
          const index = arrayOfIndexes.includes(numberOfAnswer, 0);
          if (index === false) {
            arrayOfIndexes.push(numberOfAnswer);
          } else {
            i--;
          }
        }
        for (const i of arrayOfIndexes) {
          const word = data.answers[i];
          this.arrayOfWords.push(word);
        }

        this.word = this.arrayOfWords[0];
        this.goToTheWord(this.word);
    });
    this.ngOnInit();
    this.gameOver = false;
  }

  private triggerFail(): void {

    let draws = [];
    switch (this.fail) {
      case 1:
        this.ctx.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
        draws = [
          'gallows',
          'head',
          'body',
          'rightHarm',
          'leftHarm',
          'rightLeg',
          'rightFoot',
        ];
        this.drawAMan(draws);
        break;
      case 2:
        this.ctx.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
        draws = [
          'gallows',
          'head',
          'body',
          'rightHarm',
          'leftHarm',
        ];
        this.drawAMan(draws);
        break;
      case 3:
        this.ctx.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
        draws = [
          'gallows',
          'head',
          'body',
          'rightHarm',
        ];
        this.drawAMan(draws);
        break;
      case 4:
        this.ctx.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
        draws = [
          'gallows',
          'head',
          'body',
        ];
        this.drawAMan(draws);
        break;
      case 5:
        this.ctx.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
        draws = [
          'gallows',
          'head',
        ];
        this.drawAMan(draws);
        break;
      case 6:
        this.ctx.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
        draws = [
          'gallows',
        ];
        this.drawAMan(draws);
        this.showGameOver();
        break;
    }
  }
}
