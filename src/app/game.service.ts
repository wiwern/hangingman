import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  changeComponentSubject: Subject<string>;
  constructor() {
    this.changeComponentSubject = new Subject<string>()
  }

  public changeComponent(component: string): void {
    this.changeComponentSubject.next(component);
  }
}
